<?php
class BaseSQL{

	protected $pdo;
	private $table;
	private $columns;

	public function __construct(){
		try{
			$this->pdo = new PDO(DBDRIVER.":host=".DBHOST.";dbname=".DBNAME , DBUSER, DBPWD);
		}catch(Exception $e){
			die("Erreur SQL :".$e->getMessage());
		}
		$this->table = strtolower(get_called_class());
	}

	public function setColumns() {
		$this->columns = array_diff_key(
			get_object_vars($this),
			get_class_vars(get_class())
		);
	}

	public function save(){
		$this->setColumns();

		if($this->id) {
			// UPDATE
			echo "Update";
		}else{
			// INSERT
			echo "<br>Insert</br>";
			unset($this->columns['id']);

			$columns = implode(',', array_keys($this->columns));
			$values = implode(',:', array_keys($this->columns));
			$query_string = 'INSERT INTO ' . $this->table . ' (' . $columns . ') VALUES (:' . $values . ')';
			$query = $this->pdo->prepare($query_string);

			//echo "<pre>", print_r($query_string, true), "</pre>";
			$query->execute($this->columns);
		}
	}
}